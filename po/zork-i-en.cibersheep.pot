# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the zork-i-en.cibersheep package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#: ../qml/www/lib/zvm.min.js:13
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: zork-i-en.cibersheep\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-13 17:39+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/components/AppHeader.qml:21
msgid "About"
msgstr ""

#: ../qml/components/AppHeader.qml:30
msgid "Night Mode"
msgstr ""

#: ../qml/components/About.qml:56
msgid "&Back"
msgstr ""

#: zork-i-en.desktop.in.h:1
msgid "Zork (en)"
msgstr ""
