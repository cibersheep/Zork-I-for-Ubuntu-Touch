import QtQuick 2.9
import Ubuntu.Components 1.3
import Morph.Web 0.1
import QtWebEngine 1.7

import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'zork-i-en.cibersheep'
    automaticOrientation: true
    anchorToKeyboard: true

    width: units.gu(45)
    height: units.gu(75)

    property bool nightMode: false
    property string lighterColor: "#333333"
    property string darkColor: "#efefef"

    Page {
        id: page

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        header: AppHeader {
            id: header
            title: "Zork"
        }

        WebContext {
            id: webcontext
            offTheRecord: false

            userScripts: [
                WebEngineScript {
                    id: cssinjection
                    injectionPoint: WebEngineScript.DocumentCreation
                    sourceUrl: Qt.resolvedUrl('inject.js')
                    worldId: WebEngineScript.MainWorld
                }
            ]
        }

        WebView {
            id: webview

            //This factor should be around 2.75 for mobile
            property real factor: units.gu(1) / 8.4

            anchors {
                top: header.visible ? header.bottom : parent.top
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            // TODO error page
            context: webcontext
            url: 'www/index.html'
            zoomFactor: factor
        }

        ProgressBar {
            height: units.dp(3)
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }

            showProgressPercentage: false
            value: (webview.loadProgress / 100)
            visible: (webview.loading && !webview.lastLoadStopped)
        }
    }

    HideHeader {}

    Component {
        id: aboutPage
        About {
            anchors.fill: parent
        }
    }
}
